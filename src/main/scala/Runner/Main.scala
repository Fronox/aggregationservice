package Runner

import Logic.{Jwt, LoginLogic, SeriesLogic}
import akka.actor.{Actor, ActorSystem, Props}
import akka.stream.ActorMaterializer

import scala.concurrent.Await
import scala.concurrent.duration.Duration
import scala.concurrent.ExecutionContext.Implicits.global
import com.typesafe.akka.extension.quartz.QuartzSchedulerExtension

object Main {
  implicit val actorSystem: ActorSystem = ActorSystem()
  implicit val actorMaterializer: ActorMaterializer = ActorMaterializer()
  implicit val loginLogic = new LoginLogic()
  implicit val seriesLogic = new SeriesLogic(loginLogic)

  val scheduler = QuartzSchedulerExtension(actorSystem)

  case object Tick

  class Runner(implicit val seriesLogic: SeriesLogic, loginLogic: LoginLogic) extends Actor {
    override def receive: Receive = {
      case Tick =>
        val res = Await.result(seriesLogic.getSeriesListByName("The walking dead"), Duration.Inf).head.seriesName
        println(res)
    }
  }

  object Runner {
    def props(): Props = Props(new Runner())
  }
  def main(args: Array[String]): Unit = {
    //val runner = actorSystem.actorOf(Runner.props())
    //scheduler.createSchedule("Every5Seconds", None, "*/5 * * ? * *")
    //scheduler.schedule("Every5Seconds", runner, Tick)

    val list = Await.result(seriesLogic.getSeriesListByName("The walking dead"), Duration.Inf)
    val info = Await.result(seriesLogic.getSeriesInfoById(list.head.id), Duration.Inf)
    println(info)
    //val loginLogic = new LoginLogic()

     //map(x => println(x.seriesName))
  }
}
//eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1NTQ0NzMwNjIsImlkIjoiIiwib3JpZ19pYXQiOjE1NTQzODY2NjIsInVzZXJpZCI6NTIzNDE0LCJ1c2VybmFtZSI6InNlcmp6dm9naSJ9.nLnm3Px07qq2MbSsuO8N8e0RcPOVmdMLMlH4oo86F8_20XvhsemMWP4knrvO24qIQfOVWtBlLUsLDZCgKTPYwgxh6XtlkEbnelzup1spcf2f6939Fnt8AwdeaE8PDdTw6yVYvVqnjdLtJfEF4YMHwIRbYw3S8oOvlnm-a1qg8i-GJtW_J37Q_KwjL9JrFv-BNJuSibMp8t6_Aj6ShQMYXH1vnoOvGuiVYLYMOAP1DjCdBNR89IJDupEDTxyI81qPTfeN85-m7hWbhbF3OdQfTYmo312ssMYkR8cxHANi3nnXDj-vIeNw-2zax32ziEoGqXBL75Tblya_o3-uFDAd5w
/*
curl -X GET --header 'Accept: application/json' --header 'Accept-Language: en' --header 'Authorization: Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1NTQ0NzMwNjIsImlkIjoiIiwib3JpZ19pYXQiOjE1NTQzODY2NjIsInVzZXJpZCI6NTIzNDE0LCJ1c2VybmFtZSI6InNlcmp6dm9naSJ9.nLnm3Px07qq2MbSsuO8N8e0RcPOVmdMLMlH4oo86F8_20XvhsemMWP4knrvO24qIQfOVWtBlLUsLDZCgKTPYwgxh6XtlkEbnelzup1spcf2f6939Fnt8AwdeaE8PDdTw6yVYvVqnjdLtJfEF4YMHwIRbYw3S8oOvlnm-a1qg8i-GJtW_J37Q_KwjL9JrFv-BNJuSibMp8t6_Aj6ShQMYXH1vnoOvGuiVYLYMOAP1DjCdBNR89IJDupEDTxyI81qPTfeN85-m7hWbhbF3OdQfTYmo312ssMYkR8cxHANi3nnXDj-vIeNw-2zax32ziEoGqXBL75Tblya_o3-uFDAd5w' 'https://api.thetvdb.com/search/series?name=<series name>'*/
package Logic

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.stream.ActorMaterializer
import org.json4s.jackson.Serialization.write
import Runner.Json4sSupport._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

case class UserLogin(apikey: String, username: String, userkey: String)
case class Jwt(token: String)

class LoginLogic(implicit actorSystem: ActorSystem, actorMaterializer: ActorMaterializer) {
  def getToken(): Future[Jwt] = {
    val userLogin = UserLogin("285VC7HHIR5EYL2Q", "serjzvogi", "SDMOD4Q4L3Q1CEGL")
    val loginUrl = "https://api.thetvdb.com/login"
    Http().singleRequest(
      HttpRequest(
        method = HttpMethods.POST,
        entity = HttpEntity(
          ContentTypes.`application/json`,
          write(userLogin)
        ),
        uri = Uri(loginUrl)
      )
    ).flatMap(resp => Unmarshal(resp).to[Jwt])
  }
}
